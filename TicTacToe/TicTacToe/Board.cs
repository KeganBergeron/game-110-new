﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Board
    {
        char[,] cells;
        int CurrentRow;
        int CurrentCol;
        char currentPlayer;
        public char Blank;
        public Board()
        {
            cells = new char[3, 3];
            Blank = cells[0, 0];
            CurrentRow = 1;
            CurrentCol = 1;
            currentPlayer = 'X';
        }


        internal void DisplayBoard()
        {
            Console.Clear();
            for (int col = 0; col < 3; col++)
            {
                for (int row = 0; row < 3; row++)
                {
                    if (row==CurrentRow && col==CurrentCol)
                    {
                        Console.BackgroundColor = ConsoleColor.Blue;
                    }
                    Console.Write("["+ cells[col,row] + "]");
                    Console.BackgroundColor = ConsoleColor.Black;
                }
                Console.WriteLine();
            }
        }
        internal void TakeTurn()
        {
            Console.WriteLine("Player " + currentPlayer + " its your turn");
            ConsoleKey input = Console.ReadKey().Key;
            switch (input)
            {
                case ConsoleKey.UpArrow:
                    CurrentCol = (CurrentCol+2)%3;
                    break;
                case ConsoleKey.DownArrow:
                    CurrentCol = (CurrentCol + 1) % 3;
                        break;
                case ConsoleKey.LeftArrow:
                    CurrentRow = (CurrentRow + 2) % 3;
                    break;
                case ConsoleKey.RightArrow:
                    CurrentRow = (CurrentRow + 1) % 3;
                    break;
                case ConsoleKey.Enter:
                    if (cells[CurrentCol, CurrentRow]== Blank)
                    {
                        cells[CurrentCol, CurrentRow] = currentPlayer;
                        if (currentPlayer== 'X')
                        {
                            currentPlayer = 'O';
                        }
                        else
                        {
                            currentPlayer = 'X';
                        }
                    }
                    
                    break;
                case ConsoleKey.Tab:
                    break;



            }
        }
        internal char CheckWin()
        {
            if (cells[0, 0] == cells[0, 1] && cells[0, 1] == cells[0, 2])
            {
                return cells[0, 0];
            }
            if (cells[1, 0] == cells[1, 1] && cells[1, 1] == cells[1, 2])
            {
                return cells[1, 0];
            }
            if (cells[2, 0] == cells[2, 1] && cells[2, 1] == cells[2, 2])
            {
                return cells[2, 0];
            }
            if (cells[0, 0] == cells[1, 0] && cells[1, 0] == cells[2, 0])
            {
                return cells[0, 0];
            }
            if (cells[0, 1] == cells[1, 1] && cells[1, 1] == cells[2, 1])
            {
                return cells[0, 1];
            }
            if (cells[0, 2] == cells[1, 2] && cells[1, 2] == cells[2, 2])
            {
                return cells[0, 2];
            }
            if (cells[0, 0] == cells[1, 1] && cells[1, 1] == cells[2, 2])
            {
                return cells[0, 0];
            }
            if (cells[2, 0] == cells[1, 1] && cells[1, 1] == cells[0, 2])
            {
                return cells[2, 0];
            }

            return Blank;
        }
    }
}
