﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe
{
    class Program
    {
        static void Main(string[] args)
        {
            Board game = new Board();
            bool ExitGame = false;
            while (!ExitGame)
            {
                //display board
                game.DisplayBoard();
                //take a turn
                game.TakeTurn();
                //check for win - exit
                char win = game.CheckWin();
                if (win != game.Blank)
                {
                    game.DisplayBoard();
                    Console.WriteLine("Player " + win + " wins");
                    Console.Read();
                    ExitGame = true;
                }

            }
        }
    }
}
