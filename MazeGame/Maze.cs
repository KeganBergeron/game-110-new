﻿using System;

namespace MazeGame
{
    internal class Maze
    {
        Player player = new Player(2, 1);
        public string[,] tiles =
            { {"W","W", "W", "W", "W", "W", "W", "W", "W", "W","W", },                                  
              {"W","W", "S", "W", "W", "W", "W", " ", " ", " ","W", },
              {"W"," ", " ", " ", " ", " ", "W", " ", "W", " ","W", },
              {"W"," ", "W", "W", "W", " ", " ", " ", "W", " ","W", },
              {"W"," ", "W", " ", " ", " ", "W", "W", "W", " ","W", },
              {"W"," ", "W", "$", "W", "W", " ", " ", " ", " ","W", },
              {"W"," ", "W", "W", "W", " ", " ", " ", "W", " ","W", },
              {"W"," ", " ", " ", "W", "W", " ", "W", "W", "W","W", },
              {"W"," ", "W", " ", " ", "W", " ", "D", " ", "E","W", },
              {"W"," ", "K", "W", "$", "W", "W", "W", "W", "W","W", },
              {"W","W", "W", "W", "W", "W", "W", "W", "W", "W","W", }};
                    


        internal void DisplayMaze()
        {
            Console.Clear();
            for (int y = 0; y < tiles.GetLength(0); y++)
            {
                Console.CursorVisible = false;
                Console.SetCursorPosition(0, y);
                for (int x = 0; x < tiles.GetLength(1); x++)
                {
                    if (x== player.x && y == player.y)
                    {
                        Console.BackgroundColor = ConsoleColor.Red;
                        Console.Write("P ");
                    }
                    else
                    {
                        Console.Write(tiles[y, x] + " ");
                    }                    
                }
                Console.WriteLine(" ");
            }
        }
        public void Run()
        {

            while (true)
            {
                DisplayMaze();
                int newY = player.y;
                int newx = player.x;
                int Money = 0;
                ConsoleKeyInfo movement = Console.ReadKey(true);
                if (movement.Key == ConsoleKey.W)
                {
                    newY--;
                }
                if (movement.Key == ConsoleKey.S)
                {
                    newY++;

                }
                if (movement.Key == ConsoleKey.A)
                {
                    newx--;

                }
                if (movement.Key == ConsoleKey.D)
                {
                    newx++;
                }
                if (movement.Key == ConsoleKey.Escape)
                {
                    System.Environment.Exit(-1);
                }
                if (movement.Key == ConsoleKey.Q)
                {
                    System.Environment.Exit(-1);
                }
                if (Walkable(newx, newY))
                {
                    player.x = newx;
                    player.y = newY;
                }
                if (tiles[player.y, player.x] == "K")
                {
                    tiles[player.y, player.x] = " ";
                    player.haskey = true;
                }
                if (tiles[player.y, player.x] == "$")
                {
                    Money++;
                    tiles[player.y, player.x] = " ";
                }
                if (tiles[player.y, player.x] == "E")
                {
                    if (Money == 2)
                    {
                        Console.WriteLine("You Win");
                    }
                    else
                    {
                        Console.Write("Go Back And Get The Beans $");
                    }
                }
            }

        }
        public bool Walkable(int x, int y)
        {
            if (tiles[y, x] == "W")
            {
                return false;
            }
            if (tiles[y, x] == " ")
            {
                return true;
            }
            if (tiles[y, x] == "D" && !player.haskey)
            {
                return false;
            }
            return true;
        }
    }
}